
### +(void)uploadDataWithType:(XZUploadType)type WithImg:(UIImage *)img success:(uploadSuccess)success{
    [SVProgressHUD show];
    ///zbnet
        [ZBRequestManager requestWithConfig:^(ZBURLRequest * _Nullable request) {
            request.url = @"basic/uploadFile.action?savePath=upload/hyedu/usertalk/";
            [request.uploadDatas addObject:[ZBUploadData formDataWithName:@"file" fileName:@"upload.png" mimeType:@"image/png" fileData:UIImagePNGRepresentation(img)]];
//            request.parameters = @{@"savePath":@"upload/hyedu/usertalk/"};

            request.headers = @{@"Content-Type":@"multipart/form-data"};
            request.methodType = ZBMethodTypeUpload;
        } success:^(id  _Nullable responseObject, ZBURLRequest * _Nullable request) {
    
        } failure:^(NSError * _Nullable error) {
    
        }];
        ///afn
    AFHTTPSessionManager *manager1 = [AFHTTPSessionManager manager];
    manager1.requestSerializer = [AFHTTPRequestSerializer serializer];
    manager1.responseSerializer= [AFHTTPResponseSerializer serializer];
    [manager1.requestSerializer setValue:@"multipart/form-data" forHTTPHeaderField:@"Content-Type"];
    [manager1 POST:@"http://localhost/basic/uploadFile.action?fromType=ios" parameters:@{@"accountTel":@"" ,@"accessToken":@"",@"savePath":@[HY_CircleImgSavePath,HY_UserImgSavePath,HY_FeedBackImgSavePath][type]} headers:nil  constructingBodyWithBlock:^(id<AFMultipartFormData> _Nonnull formData) {
        if (img) {
            [formData appendPartWithFileData:UIImagePNGRepresentation(img) name:@"file" fileName:@"upload.png" mimeType:@"image/png"];
        }
    } progress:^(NSProgress * _Nonnull uploadProgress) {

    } success:^(NSURLSessionDataTask * _Nonnull task, id _Nullable responseObject) {
        [SVProgressHUD dismiss];
        NSString *response = [[NSString alloc]initWithData:responseObject encoding:NSUTF8StringEncoding];
        NSLog(@"%@---%@",responseObject,manager1.requestSerializer.HTTPRequestHeaders);
        XZBaseModel *model = [XZBaseModel yy_modelWithJSON:response];
        if (model.statusCode == 2000) {
            XZUploadModel *uploadModel = [XZUploadModel yy_modelWithJSON:[[response jk_dictionaryValue] [@"data"]firstObject]];
            success?success(uploadModel.view_url ?:@""):nil;
        }else{
            [SVProgressHUD showErrorWithStatus:model.msg];
            [SVProgressHUD dismissWithDelay:2];
        }
    } failure:^(NSURLSessionDataTask * _Nullable task, NSError * _Nonnull error) {
        [SVProgressHUD dismiss];
        [SVProgressHUD showErrorWithStatus:@"网络错误"];
        [SVProgressHUD dismissWithDelay:2];
    }];
}
